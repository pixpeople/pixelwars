/** Module for outputting logs.
 * Requires: nothing
 */

// Setup top level namespace.
var PIXEL001 = PIXEL001 || {};

/**
 * Simple utility for logging.
 * 
 * Supports enabling and disabling of specific logs.
 * Will in future revisions support using different log functions to redirect
 * the logs with different log functions for different tags.
 * Currently only logs using console.log and doesn't print the tag as
 * part of the log entry, just the message.
 * Will in future revisions support enabling different log levels.
 */
PIXEL001.Log = PIXEL001.Log || (function() {
   // ===========================
   // = Private variables       =
   // ===========================

   // If new tags are enabled by default. 
   var mTagDefaultEnabled = true;

   // Each tag has an entry here that has the value true if logs are enabled for
   // that tag and the value false if they are disabled.
   // The key is the tag name.
   var mTagEnabled = {};

   var slice = Function.prototype.call.bind(Array.prototype.slice);

   // ===========================
   // = Private methods         =
   // ===========================
   
   var defaultLogFunction = function(props) {
      // Remove tag from data to be logged.
      props.args = slice(props.args, 1);
      // console requires to be called with this = console.
      // Therefore we use apply.
      switch (props.type) {
         case "l":
            console.log.apply(console, props.args);
            break;
         case "d":
            console.debug.apply(console, props.args);
            break;
         case "w":
            console.warn.apply(console, props.args);
            break;
         case "e":
            console.error.apply(console, props.args);
            break;
         default:
            console.log.apply(console, props.args);
            break;
      }
   };

   /** Checks if a tag should be logged and updates list of known tags. */
   var shouldLog = function(tag) {
      // Check it it's the first time we see this tag.
      if (undefined === mTagEnabled[tag]) {
         mTagEnabled[tag] = mTagDefaultEnabled ;
      }

      // Check if logs are disabled for tag.
      return mTagEnabled[tag];
   }

   /**
    * Returns a function that when called will call function fn with arguments
    * supplied when this curry function was called plus any new parameters
    * supplied.
    *
    * Example:
    *   var add = function(a, b) {
    *     return a + b;
    *   }
    *   var newFunction = curry(add, 1);
    *   var c = newFunction(2); // c will be 3
    *
    * @param fn  The function to curry.
    */
   var curry = function(fn) {
      // Store supplied arguments except for the function.
      var storedArgs = slice(arguments, 1);

      return function () {
         var newArgs = slice(arguments);
         var args = storedArgs.concat(newArgs);
         return fn.apply(null, args);
      };
   };

   // ===========================
   // = Public methods          =
   // ===========================

   return {

      /** Returns an array of strings with the names of all tags used so far. */
      getTags: function() {
         var tags = [];
         for (var someTag in mTagEnabled) {
            if (mTagEnabled.hasOwnProperty(someTag)) {
               tags.push(someTag);
            }
         }
         return tags;
      },

      /** Enable logs for a specific tag.
       *
       * It is possible to enable a tag before it has been used.
       *
       * @param tag  The tag for which to enable logs.
       *             If not specified all logs will be enabled.
       */
      enable: function(tag) {
         if (null === tag || undefined === tag) {
            // Enable all tags
            mTagDefaultEnabled = true;
            for (var someTag in mTagEnabled) {
               if (mTagEnabled.hasOwnProperty(someTag)) {
                  mTagEnabled[someTag] = true;
               }
            }
         } else {
            // Enable specific tag
            mTagEnabled[tag] = true;
         }
      },

      /** Disable logs for a specific tag.
       *
       * It is possible to disable a tag before it has been used.
       *
       * @param tag  The tag for which to disable logs.
       *             If not specified all logs will be disabled.
       *             If not specified new tags will be default to disabled even
       *             if some tags are later enabled with enable(tag). To change
       *             default to enabled enabled must be called with empty tag.
       */
      disable: function(tag) {
         if (null === tag || undefined === tag) {
            // Disable all tags
            mTagDefaultEnabled = false;
            for (var someTag in mTagEnabled) {
               if (mTagEnabled.hasOwnProperty(someTag)) {
                  mTagEnabled[someTag] = false;
               }
            }
         } else {
            // Disable specific tag
            mTagEnabled[tag] = false;
         }
      },

      // =============================================
      // = Log shortcut/convenience methods.         =
      // =============================================

      getLogLShortcut: function(tag) {
         return curry(this.l, tag);
      },

      /** Returns a Log.d function that automatically sets the tag.
       *
       * Example usage:
       *   var LogD = Log.getLogDShortCut("MyTag");
       *   LogD("This is my log message!");
       */
      getLogDShortcut: function(tag) {
         return curry(this.d, tag);
      },

      getLogWShortcut: function(tag) {
         return curry(this.w, tag);
      },

      getLogEShortcut: function(tag) {
         return curry(this.e, tag);
      },

      // =============================================
      // = Log methods.                              =
      // =============================================

      /** Write log with level Log.
       *
       * If logging is disabled for the tag then this method will not output
       * anything.
       * @param tag  The tag that the log message belongs to.
       * @param msg  The message to log.
       */
      l: function(tag, msg) {
         if (shouldLog(tag)) {
            defaultLogFunction({tag: tag, msg: msg, type: "l", args: arguments });
         }
      },

      /** Write log with level Debug.
       *
       * If logging is disabled for the tag then this method will not output
       * anything.
       * @param tag  The tag that the log message belongs to.
       * @param msg  The message to log.
       */
      d: function(tag, msg) {
         if (shouldLog(tag)) {
            defaultLogFunction({tag: tag, msg: msg, type: "d", args: arguments });
         }
      },

      /** Write log with level Warning.
       *
       * If logging is disabled for the tag then this method will not output
       * anything.
       * @param tag  The tag that the log message belongs to.
       * @param msg  The message to log.
       */
      w: function(tag, msg) {
         if (shouldLog(tag)) {
            defaultLogFunction({tag: tag, msg: msg, type: "w", args: arguments });
         }
      },

      /** Write log with level Error.
       *
       * If logging is disabled for the tag then this method will not output
       * anything.
       * @param tag  The tag that the log message belongs to.
       * @param msg  The message to log.
       */
      e: function(tag, msg) {
         if (shouldLog(tag)) {
            defaultLogFunction({tag: tag, msg: msg, type: "e", args: arguments });
         }
      }
   };

}());
